package test.inacap.holamundoapr4db2019.modelos;

import android.provider.BaseColumns;

public class MainDBContract {
    private MainDBContract(){}

    public static class MainDBUsuarios implements BaseColumns {
        public static final String NOMBRE_TABLA = "usuarios";
        public static final String COLUMNA_USERNAME = "username";
        public static final String COLUMNA_PASSWORD = "password";
    }
}
