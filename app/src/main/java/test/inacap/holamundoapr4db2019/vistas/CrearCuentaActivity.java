package test.inacap.holamundoapr4db2019.vistas;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import test.inacap.holamundoapr4db2019.R;
import test.inacap.holamundoapr4db2019.controladores.UsuariosControlador;

public class CrearCuentaActivity extends AppCompatActivity {

    private EditText etNombre, etPassword1, etPassword2;
    private Button btRegistrar;
    private TextView tvCancelar;

    // Conexion con la capa controlador
    private UsuariosControlador capaControlador;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_cuenta);
        this.capaControlador = new UsuariosControlador(getApplicationContext());

        this.etNombre = findViewById(R.id.etNombre);
        this.etPassword1 = findViewById(R.id.etPassword1);
        this.etPassword2 = findViewById(R.id.etPassword2);
        this.btRegistrar = findViewById(R.id.btRegistrar);
        this.tvCancelar = findViewById(R.id.tvCancelar);

        this.tvCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        this.btRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = etNombre.getText().toString().trim();
                String pass1 = etPassword1.getText().toString().trim();
                String pass2 = etPassword2.getText().toString().trim();

                if(!pass1.equals("") && pass1.equals(pass2)){

                    // Enviamos los datos a la capa controlador para que sean procesados
                    capaControlador.crearUsuario(username, pass1);
                    Toast.makeText(getApplicationContext(), "Cuenta creada", Toast.LENGTH_SHORT).show();
                    finish();
                } else Toast.makeText(getApplicationContext(), "Revise las contraseñas", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
