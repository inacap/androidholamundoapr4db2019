package test.inacap.holamundoapr4db2019;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import test.inacap.holamundoapr4db2019.controladores.UsuariosControlador;
import test.inacap.holamundoapr4db2019.vistas.CrearCuentaActivity;

public class MainActivity extends AppCompatActivity {

    // Declarar componentes globales
    private EditText etNombre, etPass;
    private Button btIngresar;
    private TextView tvCrearCuenta;


    // Conexion a capa controlador
    UsuariosControlador capaControlador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.capaControlador = new UsuariosControlador(getApplicationContext());

        this.etNombre = findViewById(R.id.etNombre);
        this.etPass = findViewById(R.id.etPassword);
        this.btIngresar = findViewById(R.id.btIngresar);
        this.tvCrearCuenta = findViewById(R.id.tvCrearCuenta);

        // Detectar click sobre un boton
        this.btIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombre = etNombre.getText().toString().trim();
                String password = etPass.getText().toString().trim();

                if(!nombre.equals("") && !password.equals("")){
                    boolean login = capaControlador.procesarLogin(nombre, password);

                    if(login){
                        Toast.makeText(getApplicationContext(), "Bienvenido", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Datos incorrectos", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        this.tvCrearCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Abrir una segunda Activity
                Intent nuevaActivity = new Intent(MainActivity.this, CrearCuentaActivity.class);
                startActivity(nuevaActivity);
            }
        });

    }
}























